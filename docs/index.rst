.. Super Secret documentation master file, created by
   sphinx-quickstart on Tue Jun  8 09:09:26 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Super Secret's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Look at all of this amazing PlantUML integration. Nary an images folder to be seen!

.. uml::

      'style options 
      skinparam monochrome true
      skinparam circledCharacterRadius 9
      skinparam circledCharacterFontSize 8
      skinparam classAttributeIconSize 0
      hide empty members

      abstract class AbstractClass {
        - privateField
        + publicField
        # protectedField
        ~ packagePrivateField
        - privateMethod()
        + publicMethod()
        # protectedMethod()
        ~ packagePrivateMethod()
         }
  
      class Dummy {
        {static} staticID
        {abstract} void methods()
         }
      
      class Flight {
         flightNumber : Integer
         departureTime : Date
         }
      
      package "Classic Collections" {
         
         abstract class AbstractList
         abstract AbstractCollection
         interface List
         interface Collection
         
         List <|-- AbstractList
         Collection <|-- AbstractCollection
         
         Collection <|- List
         AbstractCollection <|- AbstractList
         AbstractList <|-- ArrayList
         
         class ArrayList {
           Object[] elementData
           size()
            } 
      }
      
      enum TimeUnit {
        DAYS
        HOURS
        MINUTES
      }
        

      class Student {
        Name
      }
      Student "0..*" -- "1..*" Course
      (Student, Course) .. Enrollment
      
      class Enrollment {
        drop()
        cancel()
      }


And Lo! GraphViz integration too!

.. graphviz::

   digraph Test2{
      "From" -> "To";
   }

.. graphviz::

   digraph {
     A [href="URL"];
   }


.. graphviz::

   digraph Flatland {
   
      a -> b -> c -> g; 
      a  [shape=polygon,sides=4]
      b  [shape=polygon,sides=5]
      c  [shape=polygon,sides=6]
   
      g [peripheries=3,color=yellow];
      s [shape=invtriangle,peripheries=1,color=red,style=filled];
      w  [shape=triangle,peripheries=1,color=blue,style=filled];
      
      }


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
